package ru.operator.unavailablesubscriber.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.operator.unavailablesubscriber.model.Call;

import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class CallRepository {

    @Autowired
    JdbcTemplate template;

    public List<Call> getCallsByTime(LocalDateTime upperBound) {
        return template.query("SELECT caller, respondent, last_check_time, time_of_call " +
                        "  FROM CALL  c " +
                        " WHERE c.last_check_time < ? " +
                        "   AND c.time_of_call > NOW()-3 ",
                new Object[]{Timestamp.valueOf(upperBound)},
                new int[]{Types.TIMESTAMP},
                (result, rowNum) -> new Call(result.getString("caller"),
                        result.getString("respondent"),
                        result.getTimestamp("last_check_time"),
                        result.getTimestamp("time_of_call"))
        );
    }

    public int addCall(String caller, String respondent) {
        String query = "MERGE INTO call (caller, respondent, last_check_time) KEY(caller, respondent) VALUES (?, ?, NOW())";
        return template.update(query, caller, respondent);
    }

    public int delete(Call call) {
        String query = "DELETE FROM call WHERE caller = ? AND respondent = ?";
        return template.update(query, call.getCaller(), call.getRespondent());
    }

    public int deleteOld() {
        String query = "DELETE FROM call WHERE time_of_call < NOW()-3";
        return template.update(query);
    }
}
