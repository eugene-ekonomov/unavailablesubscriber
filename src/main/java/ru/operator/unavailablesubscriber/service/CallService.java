package ru.operator.unavailablesubscriber.service;

import com.google.common.util.concurrent.RateLimiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;
import ru.operator.unavailablesubscriber.model.Call;
import ru.operator.unavailablesubscriber.model.PingResult;
import ru.operator.unavailablesubscriber.model.RequestCallBody;
import ru.operator.unavailablesubscriber.model.RequestSendSmsBody;
import ru.operator.unavailablesubscriber.repository.CallRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class CallService {
    @Autowired
    private CallRepository callRepository;

    private AsyncRestTemplate asyncRestTemplate = new AsyncRestTemplate();
    private RestTemplate restTemplate = new RestTemplate();

    private AtomicBoolean inProgress = new AtomicBoolean(false);

    Map<Call, ListenableFuture<ResponseEntity<PingResult>>> smsMap = new ConcurrentHashMap<>();

    @Value("${ping.url}")
    private String pingUrl;

    @Value("${sms.url}")
    private String smsUrl;

    @Value("${sms.text}")
    private String smsText;

    private RateLimiter rateLimiter = RateLimiter.create(300.0);

    @Value("${ping.delay}")
    private int pingDelay;

    @Value("${result.success}")
    private String resultSuccess;

    @Value("${result.fail}")
    private String resultFail;

    public int addCall(RequestCallBody call) {
        return callRepository.addCall(call.getMsisdnA(), call.getMsisdnB());
    }

    public void check() {
        callRepository.deleteOld();
        LocalDateTime upperBound = LocalDateTime.now().minusMinutes(pingDelay);
        List<Call> calls = callRepository.getCallsByTime(upperBound);
        ping(calls);
    }

    private void ping(List<Call> calls) {
        if (inProgress.compareAndSet(false, true) != true) {
            return;
        }
        Map<Call, ListenableFuture<ResponseEntity<PingResult>>> futures = new ConcurrentHashMap<>();
        for (Call call : calls) {
            futures.put(call, asyncRestTemplate.getForEntity(pingUrl + "?msisdn=" + call.getRespondent(), PingResult.class));
        }
        while (futures.size() != 0) {
            for (Map.Entry<Call, ListenableFuture<ResponseEntity<PingResult>>> entry : futures.entrySet()) {
                try {
                    if (entry.getValue().isDone()) {
                        System.out.println(entry.getKey() + " " + entry.getValue().get().getBody().getStatus());
                        futures.remove(entry.getKey());
                        callRepository.delete(entry.getKey());
                        sendSms(entry.getKey());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        inProgress.set(false);
    }

    private void sendSms(Call call) {
        RequestSendSmsBody sms = new RequestSendSmsBody();
        sms.setMsisdnA(call.getRespondent());
        sms.setMsisdnB(call.getCaller());
        sms.setText(smsText);
        rateLimiter.acquire(1);
        restTemplate.postForObject(smsUrl, sms, String.class);
    }

}
