package ru.operator.unavailablesubscriber;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import ru.operator.unavailablesubscriber.service.CallService;

@Configuration
@EnableScheduling
public class AppConfig {

    @Autowired
    CallService callService;

    @Scheduled(fixedRateString = "${scheduler.delay}")
    public void work() {
        callService.check();
    }
}