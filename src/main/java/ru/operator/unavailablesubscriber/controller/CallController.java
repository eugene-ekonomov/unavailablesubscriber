package ru.operator.unavailablesubscriber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.operator.unavailablesubscriber.model.RequestCallBody;
import ru.operator.unavailablesubscriber.service.CallService;

@RestController
public class CallController {

    @Autowired
    CallService callService;

    @RequestMapping("/unavailableSubscriber")
    public void unavailableSubscriber(@RequestBody RequestCallBody call) {
        callService.addCall(call);
    }

}
