package ru.operator.unavailablesubscriber.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.operator.unavailablesubscriber.model.PingResult;
import ru.operator.unavailablesubscriber.model.RequestSendSmsBody;

@RestController
public class MockController {

    @Value("${result.success}")
    private String resultSuccess;

    @Value("${result.fail}")
    private String resultFail;

    @RequestMapping("/mockPinger")
    public PingResult mockPinger(@RequestParam String msisdn) {
        if (Integer.parseInt(msisdn.substring(msisdn.length() - 1, msisdn.length())) % 2 == 0) {
            return new PingResult(resultSuccess);
        }
        return new PingResult(resultFail);
    }

    @RequestMapping("/mockSms")
    public void mockSms(@RequestBody RequestSendSmsBody sms) {
        System.out.println(sms.getMsisdnA() + " " + sms.getMsisdnB() + " " + sms.getText());
    }
}
