package ru.operator.unavailablesubscriber.model;

import java.sql.Timestamp;

public class Call {
    private String caller;
    private String respondent;
    private Timestamp lastCheckTime;
    private Timestamp timeOfCall;

    public Call() {
    }

    public Call(String caller, String respondent, Timestamp lastCheckTime, Timestamp timeOfCall) {
        this.caller = caller;
        this.respondent = respondent;
        this.lastCheckTime = lastCheckTime;
        this.timeOfCall = timeOfCall;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public String getRespondent() {
        return respondent;
    }

    public void setRespondent(String respondent) {
        this.respondent = respondent;
    }

    public Timestamp getLastCheckTime() {
        return lastCheckTime;
    }

    public void setLastCheckTime(Timestamp lastCheckTime) {
        this.lastCheckTime = lastCheckTime;
    }

    public Timestamp getTimeOfCall() {
        return timeOfCall;
    }

    public void setTimeOfCall(Timestamp timeOfCall) {
        this.timeOfCall = timeOfCall;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Call call = (Call) o;

        if (!caller.equals(call.caller)) return false;
        return respondent.equals(call.respondent);
    }

    @Override
    public int hashCode() {
        int result = caller.hashCode();
        result = 31 * result + respondent.hashCode();
        return result;
    }
}
