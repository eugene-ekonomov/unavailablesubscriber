package ru.operator.unavailablesubscriber.model;

public class RequestCallBody {
    //caller
    private String msisdnA;
    //respondent
    private String msisdnB;

    public RequestCallBody() {

    }

    public RequestCallBody(String msisdnA, String msisdnB) {
        this.msisdnA = msisdnA;
        this.msisdnB = msisdnB;
    }

    public String getMsisdnA() {
        return msisdnA;
    }

    public void setMsisdnA(String msisdnA) {
        this.msisdnA = msisdnA;
    }

    public String getMsisdnB() {
        return msisdnB;
    }

    public void setMsisdnB(String msisdnB) {
        this.msisdnB = msisdnB;
    }
}
