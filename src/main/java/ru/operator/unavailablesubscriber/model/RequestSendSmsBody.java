package ru.operator.unavailablesubscriber.model;

public class RequestSendSmsBody {

    //respondent
    private String msisdnA;
    //caller
    private String msisdnB;
    private String text;

    public RequestSendSmsBody() {
    }

    public RequestSendSmsBody(String msisdnA, String msisdnB, String text) {
        this.msisdnA = msisdnA;
        this.msisdnB = msisdnB;
        this.text = text;
    }

    public String getMsisdnA() {
        return msisdnA;
    }

    public void setMsisdnA(String msisdnA) {
        this.msisdnA = msisdnA;
    }

    public String getMsisdnB() {
        return msisdnB;
    }

    public void setMsisdnB(String msisdnB) {
        this.msisdnB = msisdnB;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


}
