package ru.operator.unavailablesubscriber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnavailablesubscriberApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnavailablesubscriberApplication.class, args);
    }

}
