CREATE TABLE IF NOT EXISTS `call` (
                        `caller` VARCHAR(11) NULL DEFAULT NULL,
                        `respondent` VARCHAR(11) NULL DEFAULT NULL,
                        `last_check_time` TIMESTAMP  DEFAULT NOW(),
                        `time_of_call` TIMESTAMP  DEFAULT NOW(),
    PRIMARY KEY (`caller`, `respondent`)
);